const table = document.getElementById('main-table');
const tableSize = 5;
const button = document.getElementById('generate-button');
const interval = document.getElementById('interval-field');
const classes = [
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
];

button.onclick = async (e) => {
    button.setAttribute('disabled', true);
    let currentClasses = shuffle(classes);
    let value = parseInt(interval.value);
    let output = `<thead>
            <tr class="table-${currentClasses.pop()}">
                <th scope="col">1</th>
                <th scope="col">2</th>
                <th scope="col">3</th>
                <th scope="col">4</th>
                <th scope="col">5</th>
            </tr>
            </thead>
            <tbody></tbody>`;

    table.innerHTML = output;

    const even = currentClasses.pop()
    const odd = currentClasses.pop();

    let body = $('tbody');

    for (let i = 1; i <= tableSize; i++) {
        output = `<tr id="${i}" class="table-${i % 2 === 0 ? even : odd}">`;
        for (let j = 1; j <= tableSize; j++) {
            output += `<td>${Math.random().toString(36).substr(2)}</td>`;
        }
        output += '</tr>';
        body.append(output);
        await sleep(interval.value * 100);
    }

    button.removeAttribute('disabled')
};

function shuffle(array) {
    let result = array.slice(0);
    for (let i = result.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [result[i], result[j]] = [result[j], result[i]];
    }

    return result;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}