<?php

define('MYSQL_USER', 'demian');
define('MYSQL_PASSWORD', 'secret');
define('MYSQL_DATABASE', 'iteh2lb1var2');
define('MYSQL_HOST', 'mysql');

$db = new PDO('mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
return $db;