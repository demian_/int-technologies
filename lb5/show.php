<?php

/** @var PDO $dbConnection */
$dbConnection = require_once __DIR__ . '/db.php';

$queryString = "select week_day, lesson_number, auditorium, disciple, type, g.title 'group', t.name 'teacher' from lesson l left join lesson_groups lg on l.ID_Lesson = lg.FID_Lesson2 left join lesson_teacher lt on l.ID_Lesson = lt.FID_Lesson1 left join `groups` g on g.ID_Groups = lg.FID_Groups left join teacher t on t.ID_Teacher = lt.FID_Teacher";

$lessons = [];
if ($group = $_GET['group']) {
    $queryString .= ' where g.ID_Groups = :group';
    $statement = $dbConnection->prepare($queryString);
    $statement->bindParam(':group', $group);
    $statement->execute();
    $lessons = $statement->fetchAll();
} elseif ($teacher = $_GET['teacher']) {
    $queryString .= ' where t.ID_Teacher = :teacher';
    $statement = $dbConnection->prepare($queryString);
    $statement->bindParam(':teacher', $teacher);
    $statement->execute();
    $lessons = $statement->fetchAll();
} elseif ($auditorium = $_GET['auditorium']) {
    $queryString .= ' where l.auditorium = :auditorium';
    $statement = $dbConnection->prepare($queryString);
    $statement->bindParam(':auditorium', $auditorium);
    $statement->execute();
    $lessons = $statement->fetchAll();
}

if (count($lessons) > 0) {
    $preparedLessons = '';
    foreach ($lessons as $lesson) {
        $preparedLessons .= "<tr><td>${lesson['week_day']}</td><td>${lesson['lesson_number']}</td><td>${lesson['auditorium']}</td><td>${lesson['disciple']}</td><td>${lesson['type']}</td><td>${lesson['group']}</td><td>${lesson['teacher']}</td></tr>" . PHP_EOL;
    }

    echo "
<table border='1'>
    <tr>
        <th>week_day</th>
        <th>lesson_number</th>
        <th>auditorium</th>
        <th>disciple</th>
        <th>type</th>
        <th>group</th>
        <th>teacher</th>
    </tr>
    ${preparedLessons}
</table>
";
} else {
    echo 'No results!' . PHP_EOL;
}