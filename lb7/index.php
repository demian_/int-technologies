<?php

/** @var PDO $dbConnection */
$dbConnection = require_once __DIR__ . '/db.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $lessonStatement = $dbConnection->prepare('INSERT INTO `lesson` (`week_day`, `lesson_number`, `auditorium`, `disciple`, `type`) VALUES (:week_day, :lesson_number, :auditorium, :disciple, :type)');

    $lessonStatement->bindParam(":week_day", $_POST['week_day']);
    $lessonStatement->bindParam(":lesson_number", $_POST['lesson_number']);
    $lessonStatement->bindParam(":auditorium", $_POST['auditorium']);
    $lessonStatement->bindParam(":disciple", $_POST['disciple']);
    $lessonStatement->bindValue(":type", 'Practical');

    try {
        $dbConnection->beginTransaction();

        $lessonGroupStatement = $dbConnection->prepare('INSERT INTO `lesson_groups` (`FID_Lesson2`, `FID_Groups`) VALUES (:lesson, :group)');
        $teacherStatement = $dbConnection->prepare('INSERT INTO `lesson_teacher` VALUES (:teacher, :lesson)');
        $lessonStatement->execute();
        $lessonId = $dbConnection->lastInsertId();
        $lessonGroupStatement->bindParam(":lesson", $lessonId);
        $lessonGroupStatement->bindParam(":group", $_POST['group']);
        $teacherStatement->bindParam(":lesson", $lessonId);
        $teacherStatement->bindParam(":teacher", $_POST['teacher']);
        $lessonGroupStatement->execute();
        $teacherStatement->execute();
        $dbConnection->commit();
    } catch (Exception $exception) {
        $dbConnection->rollBack();
    }
}

$groups = $dbConnection->query('SELECT * FROM `groups`')->fetchAll(PDO::FETCH_ASSOC);
$teachers = $dbConnection->query('SELECT * FROM `teacher`')->fetchAll(PDO::FETCH_ASSOC);
$auditoriums = $dbConnection->query('SELECT auditorium FROM `lesson`')->fetchAll(PDO::FETCH_ASSOC);
$lessons = $dbConnection->query("select week_day, lesson_number, auditorium, disciple, type, g.title 'group', t.name 'teacher' from lesson l left join lesson_groups lg on l.ID_Lesson = lg.FID_Lesson2 left join lesson_teacher lt on l.ID_Lesson = lt.FID_Lesson1 left join `groups` g on g.ID_Groups = lg.FID_Groups left join teacher t on t.ID_Teacher = lt.FID_Teacher")->fetchAll(PDO::FETCH_ASSOC);

$preparedGroups = '';
foreach ($groups as $group) {
    $preparedGroups .= "<option value='${group['ID_Groups']}'>${group['title']}</option>" . PHP_EOL;
}

$preparedTeachers = '';
foreach ($teachers as $teacher) {
    $preparedTeachers .= "<option value='${teacher['ID_Teacher']}'>${teacher['name']}</option>" . PHP_EOL;
}

$preparedAuditoriums = '';
foreach ($auditoriums as $auditorium) {
    $preparedAuditoriums .= "<option value='${auditorium['auditorium']}'>${auditorium['auditorium']}</option>" . PHP_EOL;
}

$preparedLessons = '';
foreach ($lessons as $lesson) {
    $preparedLessons .= "<tr><td>${lesson['week_day']}</td><td>${lesson['lesson_number']}</td><td>${lesson['auditorium']}</td><td>${lesson['disciple']}</td><td>${lesson['type']}</td><td>${lesson['group']}</td><td>${lesson['teacher']}</td></tr>" . PHP_EOL;
}

echo "
<table border='1'>
    <tr>
        <th>week_day</th>
        <th>lesson_number</th>
        <th>auditorium</th>
        <th>disciple</th>
        <th>type</th>
        <th>group</th>
        <th>teacher</th>
    </tr>
    ${preparedLessons}
</table>
";

echo "<p>
    <form action='./show.php' target='_blank'>
        <b>Show schedule for group
            <select name='group' id='group'>
                ${preparedGroups}
            </select>
            <button id='groups-submit' type='submit'>Ok</button>
        </b>
    </form>
</p>";

echo "<p>
    <form action='./show.php' target='_blank'>
        <b>Show schedule for teacher
            <select name='teacher' id='teacher'>
                ${preparedTeachers}
            </select>
            <button id='teachers-submit' type='submit'>Ok</button>
        </b>
    </form>
</p>";

echo "<p>
    <form action='./show.php'>
        <b>Show schedule for auditorium
            <select name='auditorium' id='auditorium'>
                ${preparedAuditoriums}
            </select>
            <button id='auditoriums-submit' type='submit'>Ok</button>
        </b>
    </form>
</p>";

echo "
<p><b>Add new Practical Task
    <form action='' method='POST'>
        <p><input type='text' name='week_day' placeholder='Enter day of week' required></p>
        <p><input type='number' name='lesson_number' placeholder='Enter number of lesson' min='1' max='10' required style='width:160px'></p>
        <p><input type='text' name='auditorium' placeholder='Enter auditorium' required></p>
        <p><input type='text' name='disciple' placeholder='Enter disciple' required></p>
        <p>Choose teacher <select name='teacher' id='teacher'>${preparedTeachers}</select> Choose group <select name='group' id='group'>${preparedGroups}</select></p>
        <p><button type='submit'>Add</button></p>
    </form>
</b></p>";

echo '
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
$("#auditoriums-submit").on("click", function (event) {
    event.preventDefault();
    let auditorium = $("#auditorium").val();
    
    let response = fetch(`./show.php?auditorium=${auditorium}`);
    let table = $("table");
    response.then(function (response) {
        return response.json();
    }).then(function (data) {
        table.empty();
        table.append(`
        <tr>
                <th>week_day</th>
                <th>lesson_number</th>
                <th>auditorium</th>
                <th>disciple</th>
                <th>type</th>
                <th>group</th>
                <th>teacher</th>
        </tr>
        `);
        data.forEach(function (el) {
            table.append(`<tr><td>${el["week_day"]}</td><td>${el["lesson_number"]}</td><td>${el["auditorium"]}</td><td>${el["disciple"]}</td><td>${el["type"]}</td><td>${el["group"]}</td><td>${el["teacher"]}</td></tr>`);
        });
    });
});
$("#teachers-submit").on("click", function (event) {
    event.preventDefault();
    
    let teacher = $("#teacher").val();
    
    let response = fetch(`./show.php?teacher=${teacher}`);
    let table = $("table");
    response.then(function (response) {
        return response.text();
    }).then(function (data) {
        table.empty();
        
        table.append(data);
    });
});
$("#groups-submit").on("click", function (event) {
    event.preventDefault();
    
    let group = $("#group").val();
    
    let xhr = new XMLHttpRequest();
    let table = $("table");
    xhr.open("GET", `./show.php?group=${group}`);
    xhr.send();
    xhr.onload = function () {
        table.empty();
        table.append(`
        <tr>
                <th>week_day</th>
                <th>lesson_number</th>
                <th>auditorium</th>
                <th>disciple</th>
                <th>type</th>
                <th>group</th>
                <th>teacher</th>
        </tr>
        `);
        xhr.responseXML.firstChild.childNodes.forEach(function (el) {
            let result = "";
            el.childNodes.forEach(function (node) {
                result += `<td>${node.textContent}</td>`;
            });
            table.append(`<tr>${result}</tr>`);
        });
    }
});
</script>
';